# README #

### What is this repository for? ###

Settings and mapping for Elasticsearch 7.0.0

You can run config.sh to configure Elasticsearch.

You can change Index in config.sh, if you want.

### How do I get set up? ###

PUT _mappping and _settings in Elasticsearch index

### Set Index ###

Use the same settings for teams, individuals and publications. And use mappings nested_inria_authors.json for individuals, nested_researchteams for teams and publications.json for publications
